import { TypeBill } from 'src/typebills/entities/typebill.entity';
import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  dateAt: string;

  @Column()
  price: number;

  @ManyToOne(() => TypeBill, (typeBill) => typeBill.bill)
  typeBill: TypeBill[]; // ต้องเป็น TypeBill[]
}
