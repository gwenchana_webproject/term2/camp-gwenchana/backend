import { Injectable } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
  ) {}
  create(createBillDto: CreateBillDto) {
    const bill = new Bill();
    bill.dateAt = createBillDto.dateAt;
    bill.price = parseFloat(createBillDto.price);
    bill.typeBill = [];

    return this.billsRepository.save(bill);
  }

  findAll() {
    return this.billsRepository.find({ relations: { typeBill: true } });
  }

  findOne(id: number) {
    return this.billsRepository.findOne({
      where: { id },
      relations: { typeBill: true },
    });
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    const bill = await this.billsRepository.findOneOrFail({
      where: { id },
    });
    bill.dateAt = updateBillDto.dateAt;
    bill.price = parseFloat(updateBillDto.price);
    this.billsRepository.save(bill);
    const result = await this.billsRepository.findOne({
      where: { id },
      relations: { typeBill: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteBill = await this.billsRepository.findOneOrFail({
      where: { id },
    });
    await this.billsRepository.remove(deleteBill);

    return deleteBill;
  }
}
