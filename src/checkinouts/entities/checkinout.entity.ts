import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  time_in: string;

  @Column()
  time_out: string;

  @Column()
  total_hour: string;

  @ManyToOne(() => User, (user) => user.checkinout)
  @JoinColumn({ name: 'userId' })
  user: User;
}
