import { Test, TestingModule } from '@nestjs/testing';
import { CheckinoutsController } from './checkinouts.controller';
import { CheckinoutsService } from './checkinouts.service';

describe('CheckinoutsController', () => {
  let controller: CheckinoutsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckinoutsController],
      providers: [CheckinoutsService],
    }).compile();

    controller = module.get<CheckinoutsController>(CheckinoutsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
