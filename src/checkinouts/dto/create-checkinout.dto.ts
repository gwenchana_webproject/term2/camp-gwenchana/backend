import { IsNotEmpty } from 'class-validator';

export class CreateCheckinoutDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  time_in: string;

  @IsNotEmpty()
  time_out: string;

  total_hour: string;
}
