import { Module } from '@nestjs/common';
import { CheckinoutsService } from './checkinouts.service';
import { CheckinoutsController } from './checkinouts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Checkinout } from './entities/checkinout.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Checkinout])],
  controllers: [CheckinoutsController],
  providers: [CheckinoutsService],
})
export class CheckinoutsModule {}
