import { Injectable } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto) {
    const material = new Material();
    material.name = createMaterialDto.name;
    material.min = parseFloat(createMaterialDto.min);
    material.qty = parseFloat(createMaterialDto.qty);
    material.unit = JSON.parse(createMaterialDto.unit);
    if (createMaterialDto.image && createMaterialDto.image !== '') {
      material.image = createMaterialDto.image;
    }
    return this.materialsRepository.save(material);
  }

  findAll() {
    return this.materialsRepository.find({ relations: { unit: true } });
  }
  async findAllsp() {
    const results = await this.materialsRepository.query(`CALL GetStock()`);
    return results[0];
  }

  findOne(id: number) {
    return this.materialsRepository.findOne({
      where: { id },
      relations: { unit: true },
    });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialsRepository.findOneOrFail({
      where: { id },
    });
    material.name = updateMaterialDto.name;
    material.min = parseFloat(updateMaterialDto.min);
    material.qty = parseFloat(updateMaterialDto.qty);
    if (updateMaterialDto.image && updateMaterialDto.image !== '') {
      material.image = updateMaterialDto.image;
    }
    this.materialsRepository.save(material);
    const result = await this.materialsRepository.findOne({
      where: { id },
      relations: { unit: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteMaterial = await this.materialsRepository.findOneOrFail({
      where: { id },
    });
    await this.materialsRepository.remove(deleteMaterial);

    return deleteMaterial;
  }
}
