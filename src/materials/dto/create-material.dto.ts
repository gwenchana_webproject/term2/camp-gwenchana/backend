export class CreateMaterialDto {
  name: string;

  qty: string;

  min: string;

  unit: string;

  image: string;
}
