import { Injectable } from '@nestjs/common';
import { CreateUnitDto } from './dto/create-unit.dto';
import { UpdateUnitDto } from './dto/update-unit.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Unit } from './entities/unit.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UnitsService {
  constructor(
    @InjectRepository(Unit) private unitsRepository: Repository<Unit>,
  ) {}
  create(createUnitDto: CreateUnitDto) {
    return this.unitsRepository.save(createUnitDto);
  }

  findAll() {
    return this.unitsRepository.find();
  }

  findOne(id: number) {
    return this.unitsRepository.findOneBy({ id });
  }

  async update(id: number, updateUnitDto: UpdateUnitDto) {
    await this.unitsRepository.findOneByOrFail({ id });
    await this.unitsRepository.update(id, updateUnitDto);
    const updatedUnit = await this.unitsRepository.findOneBy({ id });
    return updatedUnit;
  }

  async remove(id: number) {
    const removedUnit = await this.unitsRepository.findOneByOrFail({ id });
    await this.unitsRepository.remove(removedUnit);
    return removedUnit;
  }
}
