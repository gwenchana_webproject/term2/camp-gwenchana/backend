import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { Salary } from './entities/salary.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private readonly salaryRepository: Repository<Salary>,
  ) {}

  async create(createSalaryDto: CreateSalaryDto): Promise<Salary> {
    const salary = new Salary();
    Object.assign(salary, createSalaryDto); // Map DTO to Entity

    return await this.salaryRepository.save(salary);
  }

  async findAll(): Promise<Salary[]> {
    return await this.salaryRepository.find();
  }

  findOne(id: number) {
    return this.salaryRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateSalaryDto: CreateSalaryDto): Promise<Salary> {
    const salary = await this.salaryRepository.findOneOrFail({
      where: { id },
    });
    if (!salary) {
      throw new Error(`Salary with ID ${id} not found`);
    }

    Object.assign(salary, updateSalaryDto);

    return await this.salaryRepository.save(salary);
  }

  async delete(id: number) {
    const deleteMaterial = await this.salaryRepository.findOneOrFail({
      where: { id },
    });
    await this.salaryRepository.remove(deleteMaterial);

    return deleteMaterial;
  }
}
