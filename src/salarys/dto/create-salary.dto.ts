export class CreateSalaryDto {
  name: string;
  bankAccount: string;
  salary: number;
  bonus: number;
  netSalary: number;
  status: string;
  created: Date;
  updated: Date;
}
