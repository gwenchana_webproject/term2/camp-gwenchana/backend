import { Module } from '@nestjs/common';
import { SalarysController } from './salarys.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Salary } from './entities/salary.entity';
import { SalarysService } from './salarys.service';

@Module({
  imports: [TypeOrmModule.forFeature([Salary])],
  controllers: [SalarysController],
  providers: [SalarysService],
})
export class SalarysModule {}
