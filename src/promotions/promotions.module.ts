import { Module } from '@nestjs/common';
import { PromotionService } from './promotions.service';
import { PromotionController } from './promotions.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { Product } from 'src/products/entities/product.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Promotion, Product])],
  controllers: [PromotionController],
  providers: [PromotionService],
})
export class PromotionsModule {}
