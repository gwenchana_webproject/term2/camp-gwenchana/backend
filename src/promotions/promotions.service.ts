// promotion.service.ts

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Promotion } from './entities/promotion.entity';
import { CreatePromotionDto } from './dto/create-promotion.dto';

@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private readonly promotionRepository: Repository<Promotion>,
  ) {}

  async create(createPromotionDto: CreatePromotionDto): Promise<Promotion> {
    const promotion = new Promotion();
    promotion.name = createPromotionDto.name;
    promotion.condition = createPromotionDto.condition;
    promotion.discountPrice = createPromotionDto.discountPrice;

    return await this.promotionRepository.save(promotion);
  }

  async findAll(): Promise<Promotion[]> {
    return await this.promotionRepository.find();
  }

  async findOne(id: number): Promise<Promotion> {
    return await this.promotionRepository.findOne({
      where: { id },
    });
  }

  async update(
    id: number,
    updatePromotionDto: CreatePromotionDto,
  ): Promise<Promotion> {
    const promotion = await this.promotionRepository.findOne({
      where: { id },
    });
    if (!promotion) {
      return null; // or throw an exception
    }

    promotion.name = updatePromotionDto.name;
    promotion.condition = updatePromotionDto.condition;
    promotion.discountPrice = updatePromotionDto.discountPrice;

    return await this.promotionRepository.save(promotion);
  }

  async remove(id: number): Promise<void> {
    await this.promotionRepository.delete(id);
  }
}
