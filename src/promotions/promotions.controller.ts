import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { PromotionService } from './promotions.service';
import { Promotion } from './entities/promotion.entity';
import { CreatePromotionDto } from './dto/create-promotion.dto';

@Controller('promotions')
export class PromotionController {
  constructor(private readonly promotionService: PromotionService) {}

  @Post()
  async create(
    @Body() createPromotionDto: CreatePromotionDto,
  ): Promise<Promotion> {
    return await this.promotionService.create(createPromotionDto);
  }

  @Get()
  async findAll(): Promise<Promotion[]> {
    return await this.promotionService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Promotion> {
    return await this.promotionService.findOne(Number(id));
  }

  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePromotionDto: CreatePromotionDto,
  ): Promise<Promotion> {
    return await this.promotionService.update(Number(id), updatePromotionDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<void> {
    await this.promotionService.remove(Number(id));
  }
}
