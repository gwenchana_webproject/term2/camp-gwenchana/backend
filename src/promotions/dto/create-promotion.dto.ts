export class CreatePromotionDto {
  name: string;
  condition: number;
  discountPrice: number;
}
