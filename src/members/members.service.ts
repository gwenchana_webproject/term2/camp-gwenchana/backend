import { Injectable } from '@nestjs/common';
import { CreateMemberDto } from './dto/create-member.dto';
import { UpdateMemberDto } from './dto/update-member.dto';
import { Member } from './entities/member.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class MembersService {
  findOneBy(tel: string) {
    throw new Error('Method not implemented.');
  }

  constructor(
    @InjectRepository(Member) private membersRepository: Repository<Member>,
  ) {}
  create(createMemberDto: CreateMemberDto) {
    const member = new Member();
    member.name = createMemberDto.name;
    member.tel = createMemberDto.tel;
    member.point = createMemberDto.point;
    return this.membersRepository.save(member);
  }

  findAll() {
    return this.membersRepository.find();
  }

  findOne(id: number) {
    return this.membersRepository.findOne({
      where: { id },
    });
  }

  findOneBytel(tel: string) {
    return this.membersRepository.findOne({
      where: { tel },
    });
  }

  async update(id: number, updateMemberDto: UpdateMemberDto) {
    const member = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    member.name = updateMemberDto.name;
    member.tel = updateMemberDto.tel;
    member.point = updateMemberDto.point;
    this.membersRepository.save(member);
    const result = await this.membersRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteMember = await this.membersRepository.findOneOrFail({
      where: { id },
    });
    await this.membersRepository.remove(deleteMember);

    return deleteMember;
  }
}
