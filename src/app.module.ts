import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { ProductsModule } from './products/products.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { Role } from './roles/entities/role.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { OrderList } from './ordersList/entities/orderList.entity';
import { OrderItem } from './ordersList/entities/orderItem.entity';
import { DataSource } from 'typeorm';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { MaterialsModule } from './materials/materials.module';
import { UnitsModule } from './units/units.module';
import { Member } from './members/entities/member.entity';
import { Unit } from './units/entities/unit.entity';
import { Material } from './materials/entities/material.entity';
import { MembersModule } from './members/members.module';
import { PromotionsModule } from './promotions/promotions.module';
import { OrdersModule } from './ordersList/orders.module';
import { SalarysModule } from './salarys/salarys.module';
import { Promotion } from './promotions/entities/promotion.entity';
import { CheckinoutsModule } from './checkinouts/checkinouts.module';
import { Checkinout } from './checkinouts/entities/checkinout.entity';
import { BillsModule } from './bills/bills.module';
import { TypebillsModule } from './typebills/typebills.module';
import { Bill } from './bills/entities/bill.entity';
import { TypeBill } from './typebills/entities/typebill.entity';

@Module({
  imports: [
    // TypeOrmModule.forRoot({
    //   type: 'sqlite',
    //   database: 'database.sqlite',
    //   synchronize: true,
    //   logging: false,
    //   entities: [
    //     User,
    //     Role,
    //     Type,
    //     Product,
    //     OrderList,
    //     OrderItem,
    //     Member,
    //     Unit,
    //     Material,
    //     Promotion,
    //     Checkinout,
    //     TypeBill,
    //     Bill,
    //   ],
    // }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'angsila',
      port: 3306,
      username: 'cscamp03',
      password: 'sLhWfxtpOs',
      database: 'cscamp03',
      entities: [
        User,
        Role,
        Type,
        Product,
        OrderList,
        OrderItem,
        Member,
        Unit,
        Material,
        Promotion,
        Checkinout,
        TypeBill,
        Bill,
      ],
      synchronize: true,
    }),
    RolesModule,
    TypesModule,
    ProductsModule,
    UsersModule,
    OrdersModule,
    AuthModule,
    MaterialsModule,
    UnitsModule,
    MembersModule,
    PromotionsModule,
    SalarysModule,
    CheckinoutsModule,
    BillsModule,
    TypebillsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
