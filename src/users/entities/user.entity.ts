import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import { OrderList } from 'src/ordersList/entities/orderList.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  userId: number;

  @Column()
  email: string;

  @Column({
    name: 'full_name',
    default: '',
  })
  fullName: string;

  @Column()
  password: string;

  @Column()
  gender: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Role, (role) => role.users, { cascade: true })
  @JoinTable()
  roles: Role[];

  @OneToMany(() => OrderList, (order) => order.user)
  orders: OrderList[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.user)
  checkinout: Checkinout[];
}
