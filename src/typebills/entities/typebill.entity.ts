import { Bill } from 'src/bills/entities/bill.entity';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class TypeBill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Bill, (bill) => bill.typeBill)
  bill: Bill[];
}
