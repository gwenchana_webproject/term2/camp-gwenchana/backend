import { PartialType } from '@nestjs/mapped-types';
import { CreateTypebillDto } from './create-typebill.dto';

export class UpdateTypebillDto extends PartialType(CreateTypebillDto) {}
