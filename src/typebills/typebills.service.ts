import { Injectable } from '@nestjs/common';
import { CreateTypebillDto } from './dto/create-typebill.dto';
import { UpdateTypebillDto } from './dto/update-typebill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeBill } from './entities/typebill.entity';
import { Repository } from 'typeorm';

@Injectable()
export class typebillsService {
  constructor(
    @InjectRepository(TypeBill)
    private typebillsRepository: Repository<TypeBill>,
  ) {}
  create(createtypebillDto: CreateTypebillDto) {
    return this.typebillsRepository.save(createtypebillDto);
  }

  findAll() {
    return this.typebillsRepository.find();
  }

  findOne(id: number) {
    return this.typebillsRepository.findOneBy({ id });
  }

  async update(id: number, updatetypebillDto: UpdateTypebillDto) {
    await this.typebillsRepository.findOneByOrFail({ id });
    await this.typebillsRepository.update(id, updatetypebillDto);
    const updatedtypebill = await this.typebillsRepository.findOneBy({ id });
    return updatedtypebill;
  }

  async remove(id: number) {
    const removedtypebill = await this.typebillsRepository.findOneByOrFail({
      id,
    });
    await this.typebillsRepository.remove(removedtypebill);
    return removedtypebill;
  }
}
