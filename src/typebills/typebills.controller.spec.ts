import { Test, TestingModule } from '@nestjs/testing';
import { TypebillsController } from './typebills.controller';
import { TypebillsService } from './typebills.service';

describe('TypebillsController', () => {
  let controller: TypebillsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TypebillsController],
      providers: [TypebillsService],
    }).compile();

    controller = module.get<TypebillsController>(TypebillsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
