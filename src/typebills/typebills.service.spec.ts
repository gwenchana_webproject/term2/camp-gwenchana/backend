import { Test, TestingModule } from '@nestjs/testing';
import { TypebillsService } from './typebills.service';

describe('TypebillsService', () => {
  let service: TypebillsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TypebillsService],
    }).compile();

    service = module.get<TypebillsService>(TypebillsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
