import { Module } from '@nestjs/common';
import { typebillsService } from './typebills.service';
import { TypebillsController } from './typebills.controller';
import { TypeBill } from './entities/typebill.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([TypeBill])],
  controllers: [TypebillsController],
  providers: [typebillsService],
})
export class TypebillsModule {}
