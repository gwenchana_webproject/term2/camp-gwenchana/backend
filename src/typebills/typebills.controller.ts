import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { typebillsService } from './typebills.service';
import { CreateTypebillDto } from './dto/create-typebill.dto';
import { UpdateTypebillDto } from './dto/update-typebill.dto';

@Controller('typebills')
export class TypebillsController {
  constructor(private readonly typebillsService: typebillsService) {}

  @Post()
  create(@Body() createTypebillDto: CreateTypebillDto) {
    return this.typebillsService.create(createTypebillDto);
  }

  @Get()
  findAll() {
    return this.typebillsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.typebillsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateTypebillDto: UpdateTypebillDto,
  ) {
    return this.typebillsService.update(+id, updateTypebillDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.typebillsService.remove(+id);
  }
}
