export class CreateOrderDto {
  orderItems: {
    productId: number;
    qty: number;
  }[];
  id: number;
}
